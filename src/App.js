import "./App.css";
import { Provider } from "./Components/Context/Context";
import routes from "./Components/Router/Routes";
import { useRoutes } from "hookrouter";
import Header from "./Components/Header";
import Footer from "./Components/Footer";

function App() {
  const routesResults = useRoutes(routes);
  return (
    <div>
      <Provider>
        <Header />
        {routesResults}
        <Footer />
      </Provider>
    </div>
  );
}

export default App;
