import React from "react";

const Form = () => {
  return (
    <div className="Form">
      <form>
        <label htmlFor="name">Име и презиме:</label>
        <br />
        <input
          id="name"
          type="text"
          required
          placeholder="твоето име и презиме"
        />
        <br />
        <label htmlFor="email">E-mail:</label>
        <br />
        <input id="email" type="email" required placeholder="твојот е-маил" />
        <br />
        <label htmlFor="phone">Мобилен:</label>
        <br />
        <input type="tel" id="phone" required placeholder="твојот телефон" />
        <br />
        <label htmlFor="cv">CV:</label>
        <br />
        <input type="file" id="cv" />
        <br />
        <label htmlFor="link">LinkedIn</label>
        <br />
        <input
          type="url"
          id="link"
          placeholder="линк до твојот LinkedIn профил"
        />
        <br />
        <button>АПЛИЦИРАЈ</button>
      </form>
    </div>
  );
};

export default Form;
