import Home from "../Pages/Home";
import Details from "../Pages/Details";
import LogIn from "../Pages/LogIn";

const routes = {
  "/": () => <Home />,
  "/details/:id": ({ id }) => <Details id={id} />,
  "/login": () => <LogIn />,
};

export default routes;
