import React from "react";
import { navigate } from "hookrouter";

const Header = () => {
  const redirect = () => {
    navigate(`/`);
  };
  const logInPage = () => {
    navigate("/login");
  };
  return (
    <div className="Header">
      <div className="header-inner">
        <div className="title">
          <i onClick={redirect} className="fas fa-briefcase"></i>
          <h1>ОГЛАСИ.МК</h1>
        </div>
        <div className="goToLogIn" onClick={logInPage}>
          <i className="fas fa-user">
            <span> Најави се</span>
          </i>
        </div>
      </div>
    </div>
  );
};

export default Header;
