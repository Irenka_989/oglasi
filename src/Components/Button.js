import React from "react";
import { navigate } from "hookrouter";

const Button = () => {
  const redirect = () => {
    navigate("/");
  };
  return (
    <div className="Button">
      <button id="pocetna" onClick={redirect}>
        ПОЧЕТНА
      </button>
    </div>
  );
};

export default Button;
