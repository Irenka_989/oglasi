import { createContext, useState } from "react";
import Array from "../../Array";

export const Context = createContext();
export const Provider = ({ children }) => {
  const [data, setData] = useState({ Array });

  const obj = {
    data,
    setData,
  };

  return <Context.Provider value={obj}>{children}</Context.Provider>;
};
