import React, { useContext } from "react";
import { Context } from "../Context/Context";
import Button from "../Button";
import Form from "../Form";

const Details = ({ id }) => {
  const { data } = useContext(Context);
  const currentEl = data.Array.find((el) => el.id == id);
  const {
    companyName,
    position,
    location,
    active,
    imageLogo,
    about,
    qualifications,
    responsibilities,
  } = currentEl;

  return (
    <div className="Details">
      <div className="back">
        <p id="vrabotuvanje">ВРАБОТУВАЊЕ</p>
        <Button />
      </div>
      <hr />
      <img src={`/img/${imageLogo}.png`} alt="logo" />
      <img src="/img/job.jpg" alt="job" id="job" />
      <h2>{position}</h2>
      <ul className="list">
        <li>
          <i className="fas fa-university"></i>

          <small>{companyName}</small>
        </li>
        <li>
          <i className="fas fa-map-marker-alt"></i>
          {location}
        </li>
        <li>
          <i className="fas fa-user"></i>
          <small> {position}</small>
        </li>

        <li>
          <i className="far fa-calendar-check"></i>Активен до:{" "}
          <span>{active}</span>
        </li>
      </ul>
      <hr />
      <p>{about}</p>
      <hr />

      <h4> Потребни квалификации:</h4>
      {qualifications.map((el) => (
        <li key={el}>{el}</li>
      ))}

      <hr />
      <h4>Одговорности и задачи:</h4>
      {responsibilities.map((el) => (
        <li key={el}>{el}</li>
      ))}

      <hr />
      <Form />
    </div>
  );
};

export default Details;
