import React, { useContext, useState, useEffect } from "react";
import { Context } from "../Context/Context";
import Card from "../Card";
const Home = () => {
  const { data } = useContext(Context);
  const [search, setSearch] = useState("");
  const [filteredArray, setFilteredArray] = useState(data.Array);

  useEffect(() => {
    if (search != "") {
      setFilteredArray(
        filteredArray.filter(
          (el) =>
            el.location.toLowerCase().includes(search.toLowerCase()) ||
            el.position.toLowerCase().includes(search.toLowerCase()) ||
            el.companyName.toLowerCase().includes(search.toLowerCase())
        )
      );
    } else {
      setFilteredArray(data.Array);
    }
  }, [search]);
  

  console.log(filteredArray, search);
  return (
    <>
      <input
        id="search-input"
        type="search"
        placeholder="&#128269; Пребарувај по региони, позиции, компании..."
        value={search}
        onChange={(e) => setSearch(e.target.value)}
      />
      <div className="Home">
        {filteredArray.map((el) => (
          <Card key={el.id} el={el} />
        ))}
      </div>
    </>
  );
};

export default Home;
