import React from "react";
import Button from "../Button";

const LogIn = () => {
  return (
    <div className="LogIn">
      <div className="logInTitle">
        <h2>Најави се</h2>
        <Button />
      </div>
      <div className="logIn-inner">
        <form className="logInForm">
          <label htmlFor="email">Е-маил:</label>
          <br />
          <input
            type="email"
            id="email"
            required
            placeholder="&#128231; Е-маил"
          />
          <br />
          <label htmlFor="password">Лозинка:</label>
          <br />
          <input
            type="password"
            id="password"
            required
            placeholder="&#128274; Лозинка"
          />
          <br />
          <button>НАЈАВИ СЕ</button>
        </form>
        <div className="logInSocial">
         
          <div className="linkedin">
            <i class="fab fa-linkedin fa-2x"></i>
            <a href="https://www.linkedin.com/feed/">Најави се со LinkedIn</a>
          </div>
          <div className="google">
            <i class="fab fa-google fa-2x"></i>
            <a href="https://www.google.mk/">Најави се со Google</a>
          </div>
          <div className="facebook">
            <i class="fab fa-facebook-square fa-2x"></i>
            <a href="https://mk-mk.facebook.com/">Најави се со Facebook</a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LogIn;
