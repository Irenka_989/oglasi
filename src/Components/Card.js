import React from "react";
import { navigate } from "hookrouter";

const Card = ({ el }) => {
  const redirect = () => {
    navigate(`/details/${el.id}`);
  };
  const { imageLogo, companyName, position, location, active } = el;

  return (
    <div onClick={redirect} className="Card">
      <img src={`/img/${imageLogo}.png`} alt="logo" className="logo-img" />
      <div className="card-inner">
        <p className="companyName">{companyName}</p>
        <h3 className="position">{position}</h3>
        <p>
          <i className="fas fa-map-marker-alt"></i>
          {location}
        </p>
        <p>
          <i className="far fa-calendar-check"></i>
          Активен до: <span className="active">{active}</span>
        </p>
        <button>
          Прикажи повеќе <i className="fas fa-angle-double-right"></i>
        </button>
      </div>
    </div>
  );
};

export default Card;
