import React from "react";

const Footer = (props) => {
  return (
    <div className="Footer">
      <div className="footer-inner">
        <div className="footer-about">
          <h4>ОГЛАСИ.МК</h4>
          <p>
            Вистинската работа за тебе! Најмногу работни позиции, најголем број
            на кандидати! Понуди за практична работа, стипендии и соработка со
            едни од најдобрите комании во Македонија и Европа.
          </p>
        </div>
        <div className="social">
          <h5>КОНТАКТ</h5>
          <p>
            <i className="far fa-envelope"></i>
            <a href="#" target="_blank">
              contact@oglasi.mk
            </a>
          </p>
          <p>
            <i className="fas fa-mobile-alt"></i>078/208-986
          </p>
          <a href="#" target="_blank">
          <i className="fab fa-facebook-square"></i>
          </a>
          <a href="#" target="_blank">
          <i className="fab fa-instagram"></i>
          </a>
          <a href="#" target="_blank">
            <i className="fab fa-twitter"></i>
          </a>
        </div>
      </div>
      <p className="copyRight">Copyright &copy; 2021 - Огласи.мк</p>
    </div>
  );
};

export default Footer;
